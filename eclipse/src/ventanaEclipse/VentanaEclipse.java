package ventanaEclipse;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import javax.swing.JMenu;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JTabbedPane;
import java.awt.Color;
import javax.swing.ImageIcon;

public class VentanaEclipse extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaEclipse frame = new VentanaEclipse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaEclipse() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JLabel lblNuevo = new JLabel("Nuevo");
		mnArchivo.add(lblNuevo);
		
		JLabel lblAbrir = new JLabel("Abrir");
		mnArchivo.add(lblAbrir);
		
		JLabel lblGuardar = new JLabel("Guardar");
		mnArchivo.add(lblGuardar);
		
		JLabel lblGuardarComo = new JLabel("Guardar como");
		mnArchivo.add(lblGuardarComo);
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JLabel lblDeshacer = new JLabel("Deshacer");
		mnEditar.add(lblDeshacer);
		
		JLabel lblRehacer = new JLabel("Rehacer");
		mnEditar.add(lblRehacer);
		
		JLabel lblCortar = new JLabel("Cortar");
		mnEditar.add(lblCortar);
		
		JLabel lblCopiar = new JLabel("Copiar");
		mnEditar.add(lblCopiar);
		
		JLabel lblPegar = new JLabel("Pegar");
		mnEditar.add(lblPegar);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBackground(new Color(240, 128, 128));
		toolBar.setBounds(5, 5, 424, 23);
		contentPane.add(toolBar);
		
		JButton btnVisualizar = new JButton("visualizar");
		btnVisualizar.setBackground(new Color(240, 128, 128));
		toolBar.add(btnVisualizar);
		
		JButton btnHerramientas = new JButton("herramientas");
		btnHerramientas.setBackground(new Color(240, 128, 128));
		toolBar.add(btnHerramientas);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBackground(new Color(240, 128, 128));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"jpeg", "pdf", "png", "gif", "mp3", "mp4"}));
		comboBox.setToolTipText("seleccionar");
		comboBox.setBounds(50, 38, 59, 20);
		contentPane.add(comboBox);
		
		JLabel lblTipo = new JLabel("tipo :");
		lblTipo.setBounds(15, 39, 34, 18);
		contentPane.add(lblTipo);
		
		JSlider slider = new JSlider();
		slider.setMinorTickSpacing(20);
		slider.setToolTipText("");
		slider.setBounds(235, 39, 77, 19);
		contentPane.add(slider);
		
		JLabel lblNivelDeCompresion = new JLabel("nivel de compresion");
		lblNivelDeCompresion.setBounds(119, 41, 123, 14);
		contentPane.add(lblNivelDeCompresion);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(310, 39, 29, 20);
		contentPane.add(spinner);
		
		JRadioButton rdbtnCrearCopia = new JRadioButton("crear copia");
		rdbtnCrearCopia.setBounds(25, 65, 90, 20);
		contentPane.add(rdbtnCrearCopia);
		
		JButton btnNewButton = new JButton("mas opciones...");
		btnNewButton.setBackground(new Color(128, 0, 0));
		btnNewButton.setForeground(new Color(240, 128, 128));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(119, 66, 123, 20);
		contentPane.add(btnNewButton);
		
		JCheckBox chckbxAbrirAlFinalizar = new JCheckBox("abrir al finalizar");
		chckbxAbrirAlFinalizar.setBounds(245, 66, 114, 23);
		contentPane.add(chckbxAbrirAlFinalizar);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(25, 124, 255, 101);
		contentPane.add(tabbedPane);
		
		JPanel pesta�a1 = new JPanel();
		pesta�a1.setToolTipText("pesta\u00F1a1");
		tabbedPane.addTab("pesta\u00F1a1", null, pesta�a1, null);
		
		JPanel pesta�a2 = new JPanel();
		pesta�a2.setToolTipText("pesta\u00F1a2");
		tabbedPane.addTab("pesta\u00F1a2", null, pesta�a2, null);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/com/sun/deploy/uitoolkit/impl/fx/ui/resources/image/aboutjava6.png")));
		lblNewLabel.setBounds(290, 130, 139, 72);
		contentPane.add(lblNewLabel);
	}
}
